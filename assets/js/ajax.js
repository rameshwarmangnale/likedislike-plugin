function wpwisdm_like_btn_ajax(postId, usrId) {
	var post_id = postId;
	var usr_ID = usrId;
	//alert("clicked")
	jQuery.ajax({
	  url: wpwisdm_ajax_url.ajax_url,
	  type: 'post',
	  data: {
		action: 'wpwisdm_like_btn_ajax_action',
		pid: post_id,
		uid: usr_ID
	  },
	  success: function(response) {
		jQuery("#wpwisdmAjaxResponse span").html(response);
	  }
	});
  }
//   jQuery(document).ready(function($) {
//     setInterval(function(){
//         wpwisdm_like_btn_ajax(jQuery('#postId').val(), jQuery('#usrId').val())
//     }, 2000);
// });

//   jQuery(document).ready(function($) {
// 	var post_id = postId;
// 	var usr_ID = usrId;
// 	setInterval(wpwisdm_like_btn_ajax(postId, usrId), 2000);
//  });

function wpwisdm_dislike_btn_ajax(postId, usrId) {
    var post_id = postId;
    var usr_ID = usrId;
    jQuery.ajax({
        url: wpwisdm_ajax_url.ajax_url,
        type: 'post',
        data: {
            action: 'wpwisdm_dislike_btn_ajax_action',
            pid: post_id,
            uid: usr_ID
        },
        success: function(response) {
            jQuery("#wpwisdmAjaxResponse span").html(response);
        }
    });
}
  
// jQuery(document).ready(function($) {
//     setInterval(function(){
//         wpwisdm_dislike_btn_ajax(jQuery('#postId').val(), jQuery('#usrId').val())
//     }, 2000);
// });


function updateLikeCount(postId) {
    jQuery.ajax({
        url: ajax_url,
        type: 'post',
        data: {
            action: 'wpwisdm_like_count_ajax',
            pid: postId
        },
        success: function(response) {
            jQuery("#like-count").html(response);
        }
    });
}

jQuery('#like-button').click(function(){
    var postId = jQuery(this).attr('data-post-id');
    updateLikeCount(postId);
});