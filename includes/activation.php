<?php
//
//trial code
//shortcode (tag,function name)
function my_custom_shortcode( $atts ) {
    $atts = shortcode_atts(
      array(
        'message' => 'Hello World!',
      ),
      $atts,
      'custom_shortcode'
    );
  
    return $atts['message'];
  }
  add_shortcode( 'custom_shortcode', 'my_custom_shortcode' );
  

  //hook activation
function plugin_wisdmlab_activation(){
    global $wpdb, $table_prefix;
    $wp_emp = $table_prefix. 'emp';

    $q = "CREATE TABLE IF NOT EXISTS `$wp_emp` (`ID` INT NOT NULL AUTO_INCREMENT,`name` VARCHAR(50)
    NOT NULL, `email` VARCHAR(100) NOT NULL, `status` VARCHAR(100) NOT NULL ,
    PRIMARY KEY (`ID`))";
    $wpdb->print_error();
    $wpdb->query($q);

    // $q = $wpdb->prepare("INSERT INTO `$wp_emp` (`name`, `email`, `status`) VALUES (%s, %s, %s)", 'Rameshwar', 'mangnale@gmail.com', 'yes');
    // $wpdb->query($q);

    $data = array(
        'name' => 'Rameshwar',
        'email' => 'mangnale1@gmail.com',
        'status' => 'yes'
    );

    $wpdb->insert($wp_emp,$data);

}
register_activation_hook(__FILE__, 'plugin_wisdmlab_activation');

//deactivation 
function plugin_wisdmlab_deactivation(){
    global $wpdb, $table_prefix;
    $wp_emp = $table_prefix. 'emp';

    $q = "TRUNCATE `$wp_emp`";
    $wpdb->query($q);

}
register_deactivation_hook(__FILE__, 'plugin_wisdmlab_deactivation');

//if function exit , secure from naming conflix
if(!function_exists('wpwisdm_my_plugin_function')){
    function wpwisdm_my_plugin_function(){
//
    }
}

// //admin mode
// if ( is_admin() ) {
//     // we are in admin mode
//     require_once __DIR__ . '/admin/plugin-wisdmlab-admin.php';
// }

//Create like & dislike button using filter

function wpwisdm_like_dislike_buttons($content){
    $like_btn_label = get_option('wpwisdm_like_btn_label', 'Like');
    $dislike_btn_label = get_option('wpwisdm_dislike_btn_label', 'Like');

    $user_id = get_current_user_id();
    $post_id = get_the_ID();

    $like_btn_wrap = '<div class="wpwisdm-buttons-container">';
    $like_btn = '<a href="javascript:;" onclick="wpwisdm_like_btn_ajax('.$post_id.', '.$user_id.')" class="wpwisdm-btn wpwisdm-like-btn"><span class="dashicons dashicons-thumbs-up"></span> '.$like_btn_label.'</a>';
    $dislike_btn = '<a href="javascript:;" class="wpwisdm-btn wpwisdm-dislike-btn">'.$dislike_btn_label.' <span class="dashicons dashicons-thumbs-down"></span></a>';
    $like_btn_wrap_end = '</div>';

    $wpwisdm_ajax_response = '<div id="wpwisdmAjaxResponse" class="wpwisdm-ajax-response"><span></span></div>';

    $content .= $like_btn_wrap;
    $content .= $like_btn;
    $content .= $dislike_btn;
    $content .= $like_btn_wrap_end;
    $content .= $wpwisdm_ajax_response;

    return $content;
}

add_filter('the_content', "wpwisdm_like_dislike_buttons");



//plugin for dislike 

function wpwisdm_dislike_btn_ajax_action() {
    global $wpdb;
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    $table_name = $wpdb->prefix . "wpwisdm_like_system";
    if (isset($_POST['pid']) && isset($_POST['uid'])) {

        $user_id = $_POST['uid'];
        $post_id = $_POST['pid'];

        $check_dislike = $wpdb->get_var("SELECT COUNT(*) FROM $table_name WHERE user_id='$user_id' AND post_id='$post_id' AND dislike_count=1 ");

        if ($check_dislike > 0) {
            echo "Sorry, but you already disliked this post!";
        } else {
            $wpdb->insert(
                '' . $table_name . '',
                array(
                    'post_id' => $_POST['pid'],
                    'user_id' => $_POST['uid'],
                    'dislike_count' => 1
                ),
                array(
                    '%d',
                    '%d',
                    '%d'
                )
            );
            if ($wpdb->insert_id) {
                echo "Your dislike has been recorded!";
            }
        }
    }
    wp_die();
}
add_action('wp_ajax_wpwisdm_dislike_btn_ajax_action', 'wpwisdm_dislike_btn_ajax_action');
add_action('wp_ajax_nopriv_wpwisdm_dislike_btn_ajax_action', 'wpwisdm_dislike_btn_ajax_action');



//send mail 

// add_filter( 'cron_schedules', 'wisdm_check_every_hours' );
// function wisdm_check_every_hours( $schedules ) { 
//     $schedules['every_hours'] = array(
//         'interval' => 10,
//         'display'  => esc_html__( 'Every Hours' ), );
//     return $schedules;
// }

// add_action( 'wp', 'custom_cron_job' );
// function custom_cron_job(){
//     if ( ! wp_next_scheduled( 'wisdm_woocommerce_send_email' ) ) {
//         wp_schedule_event( time(), 'every_hours', 'wisdm_woocommerce_send_email' );
//     } 
// }

// add_action('wisdm_woocommerce_send_email', 'wisdm_generate_email_cb');

// function wisdm_generate_email_cb()
// {
//     $email_subject = "Email Subject";
//     $email_content = "Email Content";
//     wp_mail('rameshwar.mangnale@wisdmlab.com', $email_subject, $email_content);
// }