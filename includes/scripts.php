<?php

if( !function_exists('wpwisdm_plugin_scripts')) {
    function wpwisdm_plugin_scripts() {

        //Plugin Ajax JS
        function my_plugin_scripts() {
            wp_enqueue_script( 'ajax-script', plugin_dir_url( __FILE__ ) . 'assets/js/ajax.js', array('jquery') );
          }
          add_action( 'wp_enqueue_scripts', 'my_plugin_scripts' );

        wp_enqueue_script('wpwisdm-ajax', WPAC_PLUGIN_DIR. './assets/js/ajax.js', 'jQuery', '1.0.0', true );

        // wp_localize_script( 'wpwisdm-ajax', 'wpwisdm_ajax_url', array(
        //     'ajax_url' => admin_url( 'admin-ajax.php' )
        // ));
    }
    add_action('wp_enqueue_scripts', 'wpwisdm_plugin_scripts');
}