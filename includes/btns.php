<?php
function wpwisdm_like_dislike_buttons($content) {

    $like_btn_label = get_option( 'wpwisdm_like_btn_label', 'Like' );
    $dislike_btn_label = get_option( 'wpwisdm_dislike_btn_label', 'Like' );

    $user_id = get_current_user_id();
    $post_id = get_the_ID();

    $like_btn_wrap = '<div class="wpwisdm-buttons-container">';
    $like_btn = '<a href="javascript:;" onclick="wpwisdm_like_btn_ajax('.$post_id.', '.$user_id.')" class="wpwisdm-btn wpwisdm-like-btn"><span class="dashicons dashicons-thumbs-up"></span> '.$like_btn_label.'</a>';
    $dislike_btn = '<a href="javascript:;" class="wpwisdm-btn wpwisdm-dislike-btn">'.$dislike_btn_label.' <span class="dashicons dashicons-thumbs-down"></span></a>';
    $like_btn_wrap_end = '</div>';
    

    $wpwisdm_ajax_response = '<div id="wpwisdmAjaxResponse" class="wpwisdm-ajax-response"><span></span></div>';

    $content .= $like_btn_wrap;
    $content .= $like_btn;
    $content .= $dislike_btn;
    $content .= $like_btn_wrap_end;
    $content .= $wpwisdm_ajax_response;

    return $content;

}
add_filter('the_content', 'wpwisdm_like_dislike_buttons');