<?php
//wp cron for add post 

add_filter( 'cron_schedules', 'example_add_cron_interval' );
function example_add_cron_interval( $schedules ) { 
    $schedules['five_seconds'] = array(
        'interval' => 5,
        'display'  => esc_html__( 'Every Five Seconds' ), );
    return $schedules;
}

function my_hourly_event_callback() {
    // code to be executed on event
    $args =[
    'post_title'=> 'new post by wp cron every 10 secs',
];
wp_insert_post($args);

}
if ( ! wp_next_scheduled( 'my_hourly_event' ) ) {
            wp_schedule_event( time(), 'hourly', 'my_hourly_event' );
        }

add_action( 'my_hourly_event', 'my_hourly_event_callback' );


//send mail 

add_filter( 'cron_schedules', 'wisdm_check_every_hours' );
function wisdm_check_every_hours( $schedules ) { 
    $schedules['every_hours'] = array(
        'interval' => 3600,
        'display'  => esc_html__( 'Every Hours' ), );
    return $schedules;
}

add_action( 'wp', 'custom_cron_job' );
function custom_cron_job(){
    if ( ! wp_next_scheduled( 'wisdm_woocommerce_send_email' ) ) {
        wp_schedule_event( time(), 'every_hours', 'wisdm_woocommerce_send_email' );
    } 
}

add_action('wisdm_woocommerce_send_email', 'wisdm_generate_email_cb');

function wisdm_generate_email_cb(){
    $email_subject = "Email Subject";
    $email_content = "Email Content";
    wp_mail('my@gmail.com', $email_subject, $email_content);
}