<?php

//top menu
function wpwisdm_settings_page_html(){
    //
    if(!is_admin()){
        return;
    }
    ?>
    <div class="wrap">
        <h1 style="font-weight: bold;"><?= esc_html(get_admin_page_title());?></h1>
        <form action="options.php" method="post">
            <?php
            settings_fields('wpwisdm-settings');
            do_settings_sections('wpwisdm-settings');
            submit_button('Save Changes')
            ?>
        </form>
    </div>
    <?
    
}
function wpwisdm_register_menu_page(){
    add_menu_page(
        'Wisdm plugin',
        'Wpwisdm Settings',
        'manage_options',
        'wpwisdm-settings',
        'wpwisdm_settings_page_html',
        'dashicons-thumbs-up',
        90
    );
}
add_action('admin_menu', 'wpwisdm_register_menu_page');

//submenu 
function wpwisdm_register_sub_menu_page(){
    add_options_page(
        'Wisdm plugin',
        'Wpwisdm Settings',
        'manage_options',
        'wpwisdm-settings',
        'wpwisdm_settings_page_html',
        90
    );
}
add_action('admin_menu', 'wpwisdm_register_sub_menu_page');

function wpwisdm_plugin_settings(){
    register_setting('wpwisdm-settings', 'wpwisdm_like_btn_label');
    register_setting('wpwisdm-settings', 'wpwisdm_dislike_btn_label');

    add_settings_section('wpwisdm_label_settings_section' ,'WPWisdm Button Label',
    'wpwisdm_plugin_settings_section_cb','wpwisdm-settings');

    add_settings_field('wpwisdm_like_label_field','Like button Label',
    'wpwisdm_like_label_field_cb', 'wpwisdm-settings','wpwisdm_label_settings_section');

    add_settings_field('wpwisdm_dislike_label_field','Dislike button Label',
    'wpwisdm_dislike_label_field_cb', 'wpwisdm-settings','wpwisdm_label_settings_section');
}
add_action('admin_init', 'wpwisdm_plugin_settings');

function wpwisdm_plugin_settings_section_cb(){
    echo '<p> Define Button Labels</p>';
}
function wpwisdm_like_label_field_cb(){
   // get the value of the setting we've registered with register_setting()
	$setting = get_option('wpwisdm_like_btn_label');
	// output the field
	?>
	<input type="text" name="wpwisdm_like_btn_label" value="<?php echo isset( $setting ) ? esc_attr( $setting ) : ''; ?>">
    <?php 
}

function wpwisdm_dislike_label_field_cb(){
    // get the value of the setting we've registered with register_setting()
     $setting = get_option('wpwisdm_dislike_btn_label');
     // output the field
     ?>
     <input type="text" name="wpwisdm_dislike_btn_label" value="<?php echo isset( $setting ) ? esc_attr( $setting ) : ''; ?>">
     <?php 
 }