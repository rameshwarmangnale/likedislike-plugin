<?php

/**
 * public wisdmlab
 * @wordpress-plugin
 * Plugin Name:       Plugin Wisdmlab
 * Plugin URI:        http://blog.local/plugin-wisdmlab
 * Description:       Wisdmlabs plugin for learning purpose.
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Rameshwar Mangnale
 * Author URI:        https://bio.link/rameshwarmangnale
 * Text Domain:       plugin-wisdmlab
 */

// “WPINC” and “ABSPATH” are constants that are defined when WordPress is loading.
//if this file is called directly , abort, stop from direct acceess
if (!defined('ABSPATH')) {
    die("");
}

//defined constant
//constant plugin version 
if (!defined('WPAC_PLUGIN_VERSION')) {
    define('WPAC_PLUGIN_VERSION', '1.0.0');
}
//plugin directory file saved, 
if (!defined('WPAC_PLUGIN_DIR')) {
    define('WPAC_PLUGIN_DIR', plugin_dir_path(__FILE__));
}

////Include Scripts & Styles
function wpwisdm_enqueue_styles()
{
    // Get the plugin directory URL
    $plugin_url = plugin_dir_url(__FILE__);

    // Enqueue the style.css file
    wp_enqueue_style('wpwisdm-style', $plugin_url . 'assets/css/style.css');
}
add_action('wp_enqueue_scripts', 'wpwisdm_enqueue_styles');

function wpwisdm_enqueue_scripts()
{
    // Get the plugin directory URL
    $plugin_url = plugin_dir_url(__FILE__);

    // Enqueue the script file
    wp_enqueue_script('wpwisdm-script', $plugin_url . 'assets/js/script.js', array(), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'wpwisdm_enqueue_scripts');

function my_plugin_scripts()
{
    wp_enqueue_script('wpwisdm-ajax', plugin_dir_url(__FILE__) . 'assets/js/ajax.js', array('jquery'));
    wp_localize_script( 'wpwisdm-ajax', 'wpwisdm_ajax_url', array(
        'ajax_url' => admin_url( 'admin-ajax.php' )
    ));
}
add_action('wp_enqueue_scripts', 'my_plugin_scripts');



//Settings menu and page
require plugin_dir_path(__FILE__) . 'includes/settings.php';

//Create table for our plugin
require plugin_dir_path(__FILE__) . 'includes/db.php';
register_activation_hook(__FILE__, 'wpwisdm_likes_table');

// Create Like & Dislike Buttons using filter.
function wpwisdm_like_dislike_buttons($content)
{

    $like_btn_label = get_option('wpwisdm_like_btn_label', 'Like');
    $dislike_btn_label = get_option('wpwisdm_dislike_btn_label', 'Like');

    $user_id = get_current_user_id();
    $post_id = get_the_ID();

    $like_btn_wrap = '<div class="wpwisdm-buttons-container">';
    $like_btn = '<a href="javascript:;" onclick="wpwisdm_like_btn_ajax(' . $post_id . ', ' . $user_id . ')" class="wpwisdm-btn wpwisdm-like-btn"><span class="dashicons dashicons-thumbs-up"></span> ' . $like_btn_label . '</a>';
    $dislike_btn = '<a href="javascript:;" onclick="wpwisdm_dislike_btn_ajax(' . $post_id . ', ' . $user_id . ')" class="wpwisdm-btn wpwisdm-dislike-btn"><span class="dashicons dashicons-thumbs-down"></span> ' . $dislike_btn_label . '</a>';
    // $dislike_btn = '<a href="javascript:;" class="wpwisdm-btn wpwisdm-dislike-btn">' . $dislike_btn_label . ' <span class="dashicons dashicons-thumbs-down"></span></a>';
    $like_btn_wrap_end = '</div>';


    $wpwisdm_ajax_response = '<div id="wpwisdmAjaxResponse" class="wpwisdm-ajax-response"><span></span></div>';

    $content .= $like_btn_wrap;
    $content .= $like_btn;
    $content .= $dislike_btn;
    $content .= $like_btn_wrap_end;
    $content .= $wpwisdm_ajax_response;

    return $content;
}
add_filter('the_content', 'wpwisdm_like_dislike_buttons');

//Wpwisdm plugin ajax function

//wpwisdm Plugin Ajax Function like
function wpwisdm_like_btn_ajax_action()
{

    global $wpdb;
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    $table_name = $wpdb->prefix . "wpwisdm_like_system";
    if (isset($_POST['pid']) && isset($_POST['uid'])) {

        $user_id = $_POST['uid'];
        $post_id = $_POST['pid'];

        $check_like = $wpdb->get_var("SELECT COUNT(*) FROM $table_name WHERE user_id='$user_id' AND post_id='$post_id' AND like_count=1 ");

        if ($check_like > 0) {
            echo "Sorry, but you already liked this post!";
        } else {
            $wpdb->insert(
                '' . $table_name . '',
                array(
                    'post_id' => $_POST['pid'],
                    'user_id' => $_POST['uid'],
                    'like_count' => 1
                ),
                array(
                    '%d',
                    '%d',
                    '%d'
                )
            );
            if ($wpdb->insert_id) {
                echo "Thank you for loving this post!";
            }
        }
    }
    wp_die();
}
add_action('wp_ajax_wpwisdm_like_btn_ajax_action', 'wpwisdm_like_btn_ajax_action');
add_action('wp_ajax_nopriv_wpwisdm_like_btn_ajax_action', 'wpwisdm_like_btn_ajax_action');

//ajax function for dislike 

function wpwisdm_dislike_btn_ajax_action() {
    global $wpdb;
    require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

    $table_name = $wpdb->prefix . "wpwisdm_like_system";
    if (isset($_POST['pid']) && isset($_POST['uid'])) {

        $user_id = $_POST['uid'];
        $post_id = $_POST['pid'];

        $check_dislike = $wpdb->get_var("SELECT COUNT(*) FROM $table_name WHERE user_id='$user_id' AND post_id='$post_id' AND dislike_count=1 ");

        if ($check_dislike > 0) {
            echo "Sorry, but you already disliked this post!";
        } else {
            //check if user has already liked the post
            $check_like = $wpdb->get_var("SELECT COUNT(*) FROM $table_name WHERE user_id='$user_id' AND post_id='$post_id' AND like_count=1 ");
            if ($check_like > 0) {
                //decrement like count by 1
                $wpdb->query("UPDATE $table_name SET like_count = like_count - 1 WHERE user_id='$user_id' AND post_id='$post_id'");
            }

            $wpdb->insert(
                '' . $table_name . '',
                array(
                    'post_id' => $_POST['pid'],
                    'user_id' => $_POST['uid'],
                    'dislike_count' => 1
                ),
                array(
                    '%d',
                    '%d',
                    '%d'
                )
            );
            if ($wpdb->insert_id) {
                echo "Your dislike has been recorded!";
            }
        }
    }
    wp_die();
}
add_action('wp_ajax_wpwisdm_dislike_btn_ajax_action', 'wpwisdm_dislike_btn_ajax_action');
add_action('wp_ajax_nopriv_wpwisdm_dislike_btn_ajax_action', 'wpwisdm_dislike_btn_ajax_action');



function wpwisdm_show_like_count($content)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "wpwisdm_like_system";
    $post_id = get_the_ID();
    $like_count = $wpdb->get_var("SELECT COUNT(*) FROM $table_name WHERE post_id='$post_id' AND like_count=1 ");
    $like_count_result = "<center>This post has been liked <strong>" . $like_count . "</strong>, time(s)</center>";
    $content .= $like_count_result;

    return $content;
}
add_filter('the_content', 'wpwisdm_show_like_count');



?>